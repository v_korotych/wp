//SLIDER
;(function($) {
    //debugger;
    var time = 2;
    var $bar,
        $slick,
        isPause,
        tick,
        percentTime;
    // Get titles from the DOM
    var $slick  = $(".vertical-slider");
    var titleSubs  = $slick.find("slick-active");
    console.log("slick", $slick);
    if ($slick.length) {

        $slick.slick({
            autoplay: true,
            arrows: true,
            centerMode: true,
            dots: true,
            slidesToShow: 10,
            centerPadding: "15px",
            draggable: true,
            infinite: true,
            pauseOnHover: true,
            swipe: false,
            touchMove: false,
            vertical: true,
            speed: 2000,
            autoplaySpeed: 4000,
            adaptiveHeight: true,
            asNavFor: '.fullscreen-slider',
            focusOnSelect: true,
            arrows: false
        });

        $bar = $('.slider-progress .progress');
        console.log("bar", $bar);
        $('.vertical-slider').on({
            mouseenter: function() {
                isPause = true;
            },
            mouseleave: function() {
                isPause = false;
            }
        })

        // On init
        $(".slick-dupe").each(function(index, el) {
            $(".vertical-slider").slick('slickAdd', "<div>" + el.innerHTML + "</div>");
        });

        // Manually refresh positioning of slick
        $slick.slick('slickPlay');
    };

    $('.fullscreen-slider').slick({
        autoplay: false,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        //asNavFor: '.vertical-slider'
    })


    var allNum = $(".vertical-slider__photo").length;
    $(".vertical-slider__photo").each(function(index, item){
        console.log(index + ", " + item.src);
    })

    console.log("Всего: ",allNum);

    // On before slide change
   /* $('.your-element').on('beforeChange', function(event, slick, currentSlide, nextSlide){
        console.log(nextSlide);
    });*/


    $.fn.clickToggle = function(func1, func2) {
        var funcs = [func1, func2];
        this.data('toggleclicked', 0);
        this.click(function() {
            var data = $(this).data();
            var tc = data.toggleclicked;
            $.proxy(funcs[tc], this)();
            data.toggleclicked = (tc + 1) % 2;
        });
        return this;
    };

    $('.btnPlay').clickToggle(function() {
        $(".vertical-slider").slick('slickPlay');
        console.log("Play");
    }, function() {
        $(".vertical-slider").slick('slickPause');
        console.log("Pause");
    });


    // startProgressbar();

    //PROGRESS BAR
    /*function startProgressbar() {
        resetProgressbar();
        percentTime = 0;
        isPause = false;
        tick = setInterval(interval, 10);
    }

    function interval() {
        if(isPause === false) {
            percentTime += 1 / (time+0.1);
            console.log("bar:" + $bar);
            $bar.css({
                width: percentTime+"%"
            });
            if(percentTime >= 100)
            {
                $slick.slick('slickNext');
                startProgressbar();
            }
        }
    }


    function resetProgressbar() {
        $bar.css({
            width: 0+'%'
        });
        clearTimeout(tick);
    }*/


})(jQuery);

;(function ($) {
    $(document).ready(function () {
        var primaryColor = "#86b11e";
        var animating = true;
        //LOGO
        logoBlock = Snap('#logo');
        console.log(logoBlock);
        Snap.load('/wp-content/themes/folio/images/svg/logo.svg', function (f) {
            var logoText = f.select("#logo-text");
            logoText
                .attr({
                    fill: "none",
                    stroke: primaryColor
                });

            var logoDot = logoBlock.circle(100, 50, 7);
            logoDot.attr({
                fill:primaryColor,
                transform:"s.8"
            });

                paintStroke(logoText,function(logoText){
                    setTimeout(function() {
                        logoText.animate({
                            fill:"#000",
                            stroke:"none"
                        }, 100, mina.easeinout());
                        addShadow(logoBlock, logoText);
                    }, 4500);
                });


            logoBlock.append(logoText);
        });


        var buttonFilter = $(".btn-filter");
        buttonFilter.first().addClass("active");
        //buttonFilter.eq(0).addClass("active");
        buttonFilter.on("click", function(){
            $(this).addClass("active").siblings().removeClass("active");
        });


        //FILTERS
        function addBlur() {
            var f = this.filter(Snap.filter.blur(5, 10)),
                filterChild = f.node.firstChild;
            this.attr({filter: f});
            Snap.animate(0, .8, function (value) {
                filterChild.attributes[0].value = value + ',' + value;
            }, 100);
        };


        function addShadow(svg, el) {
            var f = svg.filter(Snap.filter.shadow(0, 0, 20, "#ccc", 1));
            el.attr({
                filter: f
            })
        };

        function paintStroke(el, callback) {
            var pathLength = el.getTotalLength(); //Get full path length
            el.attr({
                'stroke-dasharray': '' + pathLength + ' 0'
            });

            Snap.animate(0, pathLength, function (value) {
                el.attr(
                    {'stroke-dasharray': '' + value + ' ' + (pathLength - value)});
            }, 12000);
            if(typeof(callback)==="function")
            callback(el);
        }

    });
})(jQuery);