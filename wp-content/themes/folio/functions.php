<?php
/**
 * Folio functions and definitions
 *
 * Set up the theme and provides some helper functions, which are used in the
 * theme as custom template tags. Others are attached to action and filter
 * hooks in WordPress to change core functionality.
 *
 * When using a child theme you can override certain functions (those wrapped
 * in a function_exists() call) by defining them first in your child theme's
 * functions.php file. The child theme's functions.php file is included before
 * the parent theme's file, so the child theme functions would be used.
 *
 * @link https://codex.wordpress.org/Theme_Development
 * @link https://codex.wordpress.org/Child_Themes
 *
 * Functions that are not pluggable (not wrapped in function_exists()) are
 * instead attached to a filter or action hook.
 *
 * For more information on hooks, actions, and filters,
 * {@link https://codex.wordpress.org/Plugin_API}
 *
 * @package WordPress
 * @subpackage Folio
 * @since Folio 1.0
 */

/**
 * Set the content width based on the theme's design and stylesheet.
 *
 * @since Folio 1.0
 */


/**
 * Register widget area.
 *
 * @since Folio 1.0
 *
 * @link https://codex.wordpress.org/Function_Reference/register_sidebar
 */
function folio_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Widget Area', 'folio' ),
		'id'            => 'sidebar-1',
		'description'   => __( 'Add widgets here to appear in your sidebar.', 'folio' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<div class="widget-title main-menu__title">',
		'after_title'   => '</div>',
	) );
}
add_action( 'widgets_init', 'folio_widgets_init' );

//support menu
register_nav_menus( array(
	'footer_menu' => 'Меню в футере'
) );

//Миниатюры
add_theme_support('post-thumbnails');

function folio_scripts(){
	wp_enqueue_style( 'vendor', get_template_directory_uri() . "/css/vendor.css");
	wp_enqueue_style( 'style', get_template_directory_uri() . "/css/style.css");
	wp_enqueue_script("vendor.js", get_template_directory_uri() . "/js/vendor.js");
	wp_enqueue_script("common.js", get_template_directory_uri() . "/js/common.js");
}

add_action( 'wp_enqueue_scripts', 'folio_scripts' );


