<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "site-content" div and all content after.
 *
 * @package WordPress
 * @subpackage Folio
 * @since Folio 1.0
 */
?>


<footer class="mainfooter">
	<div class="slider-progress">
		<div class="progress"></div>
	</div>
	<div class="mainfooter__container">
		<div class="mainfooter__row">
			<div class="mainfooter__nav">
				<nav class="navbar navbar-default" role="navigation">
					<!-- Brand and toggle get grouped for better mobile display -->
					<div class="navbar-header">
						<button type="button" class="navbar-toggle offcanvas-toggle pull-left" data-toggle="offcanvas" data-target="#js-bootstrap-offcanvas" style="float:left;">
							<span class="sr-only">Toggle navigation</span>
							<span>
							  <span class="icon-bar"></span>
							  <span class="icon-bar"></span>
							  <span class="icon-bar"></span>
							</span>
						</button>
					</div>
				<?php $menu = wp_nav_menu( array(
					'echo' => 0,
					'container'       => 'div',
					'container_id'   => 'js-bootstrap-offcanvas',
					'container_class' => 'navbar-offcanvas navbar-offcanvas-touch',
					'depth' => 0,
					'theme_location' => 'footer_menu',
					'menu_class' => 'nav navbar-nav'
				) );
				$menu = str_replace('class="menu-item', 'class="menu-item nav__item', $menu);
				$menu = str_replace('class="menu-item', 'class="menu-item nav__item', $menu);
				echo $menu;
				?>
				</nav>
			</div>
		</div>
		<div class="copyrights mainfooter__copyrights">
			<span class="copyrights__text">Copyright Folio 2012. All Rights Reserved.</span>
		</div>
	</div><!-- /.container-->
</footer>
</div><!-- .site -->

<?php wp_footer(); ?>
</body>
</html>
