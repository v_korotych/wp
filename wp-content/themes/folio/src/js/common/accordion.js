//ACCORDION
;(function($) {
    $(".accordion").accordion({
//whether the first section is expanded or not
        firstChildExpand: true,
//whether expanding mulitple section is allowed or not
        multiExpand: true,
//slide animation speed
        slideSpeed: 500,
//drop down icon
        dropDownIcon: "&#9660",
    });
})(jQuery);