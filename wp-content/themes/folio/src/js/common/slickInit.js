//SLIDER
;(function($) {
    //debugger;
    var time = 2;
    var $bar,
        $slick,
        isPause,
        tick,
        percentTime;
    // Get titles from the DOM
    var $slick  = $(".vertical-slider");
    var titleSubs  = $slick.find("slick-active");
    console.log("slick", $slick);
    if ($slick.length) {

        $slick.slick({
            autoplay: true,
            arrows: true,
            centerMode: true,
            dots: true,
            slidesToShow: 10,
            centerPadding: "15px",
            draggable: true,
            infinite: true,
            pauseOnHover: true,
            swipe: false,
            touchMove: false,
            vertical: true,
            speed: 2000,
            autoplaySpeed: 4000,
            adaptiveHeight: true,
            asNavFor: '.fullscreen-slider',
            focusOnSelect: true,
            arrows: false
        });

        $bar = $('.slider-progress .progress');
        console.log("bar", $bar);
        $('.vertical-slider').on({
            mouseenter: function() {
                isPause = true;
            },
            mouseleave: function() {
                isPause = false;
            }
        })

        // On init
        $(".slick-dupe").each(function(index, el) {
            $(".vertical-slider").slick('slickAdd', "<div>" + el.innerHTML + "</div>");
        });

        // Manually refresh positioning of slick
        $slick.slick('slickPlay');
    };

    $('.fullscreen-slider').slick({
        autoplay: false,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        //asNavFor: '.vertical-slider'
    })


    var allNum = $(".vertical-slider__photo").length;
    $(".vertical-slider__photo").each(function(index, item){
        console.log(index + ", " + item.src);
    })

    console.log("Всего: ",allNum);

    // On before slide change
   /* $('.your-element').on('beforeChange', function(event, slick, currentSlide, nextSlide){
        console.log(nextSlide);
    });*/


    $.fn.clickToggle = function(func1, func2) {
        var funcs = [func1, func2];
        this.data('toggleclicked', 0);
        this.click(function() {
            var data = $(this).data();
            var tc = data.toggleclicked;
            $.proxy(funcs[tc], this)();
            data.toggleclicked = (tc + 1) % 2;
        });
        return this;
    };

    $('.btnPlay').clickToggle(function() {
        $(".vertical-slider").slick('slickPlay');
        console.log("Play");
    }, function() {
        $(".vertical-slider").slick('slickPause');
        console.log("Pause");
    });


    // startProgressbar();

    //PROGRESS BAR
    /*function startProgressbar() {
        resetProgressbar();
        percentTime = 0;
        isPause = false;
        tick = setInterval(interval, 10);
    }

    function interval() {
        if(isPause === false) {
            percentTime += 1 / (time+0.1);
            console.log("bar:" + $bar);
            $bar.css({
                width: percentTime+"%"
            });
            if(percentTime >= 100)
            {
                $slick.slick('slickNext');
                startProgressbar();
            }
        }
    }


    function resetProgressbar() {
        $bar.css({
            width: 0+'%'
        });
        clearTimeout(tick);
    }*/


})(jQuery);
