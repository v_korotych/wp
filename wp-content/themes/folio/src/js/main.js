;(function ($) {
    $(document).ready(function () {
        var primaryColor = "#86b11e";
        var animating = true;
        //LOGO
        logoBlock = Snap('#logo');
        console.log(logoBlock);
        Snap.load('/wp-content/themes/folio/images/svg/logo.svg', function (f) {
            var logoText = f.select("#logo-text");
            logoText
                .attr({
                    fill: "none",
                    stroke: primaryColor
                });

            var logoDot = logoBlock.circle(100, 50, 7);
            logoDot.attr({
                fill:primaryColor,
                transform:"s.8"
            });

                paintStroke(logoText,function(logoText){
                    setTimeout(function() {
                        logoText.animate({
                            fill:"#000",
                            stroke:"none"
                        }, 100, mina.easeinout());
                        addShadow(logoBlock, logoText);
                    }, 4500);
                });


            logoBlock.append(logoText);
        });


        var buttonFilter = $(".btn-filter");
        buttonFilter.first().addClass("active");
        //buttonFilter.eq(0).addClass("active");
        buttonFilter.on("click", function(){
            $(this).addClass("active").siblings().removeClass("active");
        });


        //FILTERS
        function addBlur() {
            var f = this.filter(Snap.filter.blur(5, 10)),
                filterChild = f.node.firstChild;
            this.attr({filter: f});
            Snap.animate(0, .8, function (value) {
                filterChild.attributes[0].value = value + ',' + value;
            }, 100);
        };


        function addShadow(svg, el) {
            var f = svg.filter(Snap.filter.shadow(0, 0, 20, "#ccc", 1));
            el.attr({
                filter: f
            })
        };

        function paintStroke(el, callback) {
            var pathLength = el.getTotalLength(); //Get full path length
            el.attr({
                'stroke-dasharray': '' + pathLength + ' 0'
            });

            Snap.animate(0, pathLength, function (value) {
                el.attr(
                    {'stroke-dasharray': '' + value + ' ' + (pathLength - value)});
            }, 12000);
            if(typeof(callback)==="function")
            callback(el);
        }

    });
})(jQuery);