<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * e.g., it puts together the home page when no home.php file exists.
 *
Натяжка шаблона на WordPress | #2
 * Learn more: {@link https://codex.wordpress.org/Template_Hierarchy}
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>
					<?php if(is_front_page()) : ?>
					<div class="featured">
						<div class="featured-title"><span>Featured</span></div>
						<div class="featured-subtitle"><span>Wedding Photography Shoot</span></div>
					</div>
					<?php else : ?>
					<main class="main">
						<div class="content">
							<div class="slick-next"><i class="fa fa-chevron-down" aria-hidden="true"></i></div>
							<?php if(have_posts()) : while ( have_posts() ) : the_post(); ?>
							<article class="section">
								<h2 class="section__title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
								<span class="section__date"><?php the_date('d,.m.y'); ?></span>

								<p class="section__description"><?php the_content(); ?></p>
								<?php the_excerpt(); ?>
								<div class="tags"><?php the_tags('<span>Теги:</span> '); ?></div>
							</article>
							<?php endwhile; ?>
								<!-- post navifation-->
							<?php else: ?>
								<h3>Информация не найдена</h3>
							<?php endif; ?>
						</div>
					</main>
					<?php endif; ?>
				</div>
			</div>
		</div>
		<div class="galleryBlock">
			<div class="vertical-slider-container">
				<button class="btnPlay"><i class="fa fa-pause" aria-hidden="true"></i></button>
				<div class="vertical-slider-container__counter">
					<span class="currentNum"></span>/<span class="allNum"></span>
				</div>
				<div class="vertical-slider slider">
					<?php for($i=1; $i <= 10; $i++) { ?>
					<div class="vertical-slider__item">
						<img class="vertical-slider__photo" src="<?php echo get_template_directory_uri() ?> /images/gallery/thumbnails/<?php echo $i; ?>.jpg" alt="<?php echo $i; ?>">
					</div>
					<?php } ?>
				</div>
			</div>
		</div>

</div>

<?php get_footer();
