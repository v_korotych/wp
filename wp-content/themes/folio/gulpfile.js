var gulp = require('gulp'),
    browserSync = require('browser-sync').create(),
    sass = require('gulp-sass'),
    gulp = require('gulp'),
    nunjucksRender = require('gulp-nunjucks-render'),
    useref = require('gulp-useref'),
    uglify = require('gulp-uglify'),
    minifyCss = require('gulp-minify-css'),
    sourcemaps = require('gulp-sourcemaps'),
    autoprefixer = require('gulp-autoprefixer'),
    del = require('del'),
    concat = require('gulp-concat');


//TEXT TO SVG

/*
var TextToSVG = require('text-to-svg');

gulp.task('textToSVG', function () {
    const textToSVG = TextToSVG.loadSync('src/fonts/Pacifico/pacifico.otf');

    const attributes = {fill: 'transparent', stroke: 'black'};
    const options = {x: 0, y: 0, fontSize: 40, anchor: 'top', attributes: attributes};

    const svg = textToSVG.getSVG('Folio', options);

    console.log(svg);
});
*/



// Static server
/*gulp.task('browser-sync', function () {
    browserSync.init({
        server: {
            baseDir: "dist/"
        }
    });
});*/


//html
/*gulp.task('html', ['template'], function () {
    return gulp.src('dist/!*.html')
        .pipe(sourcemaps.init())
        .pipe(useref())
        .pipe(gulp.dest('dist/'))
        .pipe(browserSync.stream());
});*/


/*gulp.task('template', function () {
    return gulp.src('src/!*.html')
        .pipe(nunjucksRender({
            path: ['src/template/'] // String or Array
        }))
        .pipe(gulp.dest('dist/'));
});*/


//css
gulp.task('css', function () {
    gulp.src(['scss/*.scss'])
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer('last 5 version', 'ie 9'))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('css'))
        .pipe(browserSync.stream());
});


gulp.task('concat-scripts', function () {
 return gulp.src('src/template/scripts.html')
 .pipe(sourcemaps.init())
 .pipe(useref())
 .pipe(gulp.dest('src/'))
 .pipe(browserSync.stream());
 });

//img
gulp.task('img', function () {
    gulp.src('src/images/**/*.*')
        .pipe(gulp.dest('images'))
        .pipe(browserSync.stream());
});


//js
gulp.task('js', function () {
    gulp.src('src/js/**/*.*')
        .pipe(gulp.dest('js'))
        .pipe(browserSync.stream());
});


//fonts
gulp.task('fonts', function () {
    gulp.src('src/fonts/**/*.*')
        .pipe(gulp.dest('fonts/'))
        .pipe(browserSync.stream());
});

/*//slick fonts
gulp.task('slick-fonts', function () {
    gulp.src('src/fonts/slick/!*.*')
        .pipe(gulp.dest('dist/css/fonts'))
});*/


// clean
gulp.task('clean', function () {
    return del('style.css','js/*');
});


// watch
gulp.task('watch', function () {
    gulp.watch('src/*.html', ['html']);
    gulp.watch('src/stylesheets/**/*.scss', ['css']);
    gulp.watch('src/img/**', ['img']);
    gulp.watch('src/fonts/**', ['fonts']);
});


//default
gulp.task('default', [/*'browser-sync', /*'js-bower-components',*/ 'js', /*'html',*/ 'css', 'img',  /*'fonts',*/ /*'textToSVG',*/ 'watch']);
