<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Folio
 * @since Folio 1.0
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<title><?php bloginfo('name'); ?><?php wp_title('|'); ?></title>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<link rel='stylesheet' id='main-style'  href='<?php echo get_stylesheet_uri(); ?>' type='text/css' media='all' />
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?> >
<div id="fullpage">
<div class="fullscreenSliderBlock">
	<div class="fullscreen-slider slider">
		<?php for($i=1; $i <= 10; $i++) { ?>
		<div class="fullscreen-slider__item" style="background-image:url(<?php bloginfo("template_url"); ?>/images/gallery/<?php echo $i; ?>.jpg)"></div>
		<?php } ?>
	</div>
</div>
<div class="l-container">
	<div class="l-row">
		<div class="leftblock">
			<div class="l-row">
				<div class="logo_block">
                	<a class="logo_block-inner" href="<?php echo home_url(); ?>">
	    	            <svg class="logo_block-title" id="logo" width="100%" height="70px"></svg>
		                <div class="logo_block-subtitle">Photography & Design</div>
	                </a>
                </div>
			</div>
